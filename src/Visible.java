/**
 * Нечто, на что можно взглянуть.
 */
public interface Visible extends Named {
    /**
     * Определяет, вызывает ли смех у коротышек данный предмет.
     *
     * @return <code>true</code>, если объект смешной, иначе <code>false</code>.
     */
    default boolean isFunny() {
        // По умолчанию все объекты считаются несмешными.
        return false;
    }
}
