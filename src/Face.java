/**
 * Лицо коротышки.
 */
public class Face implements Visible {
    /**
     * Коротышка, чьим лицом является данный объект.
     */
    private final Shorty owner;

    /**
     * Является ли лицо чистым.
     */
    private boolean clean;

    public Face(Shorty shorty) {
        this.owner = shorty;
        this.clean = true;
    }

    /**
     * Испачкать лицо.
     */
    public void makeDirty() {
        if (clean) {
            this.clean = false;
            System.out.printf("%s где-то пачкается.\n", owner.getName());
        }
    }

    /**
     * Очистить лицо от грязи.
     */
    public void clear() {
        if (!clean) {
            this.clean = true;
            System.out.printf("%s умылся.\n", owner.getName());
        }
    }

    /**
     * Определяет, является ли лицо чистым.
     *
     * @return <code>true</code>, если лицо чистое, иначе <code>false</code>.
     */
    public boolean isClean() {
        return this.clean;
    }

    public boolean isFunny() {
        // Если лицо грязное, оно вызывает смех.
        return !isClean();
    }

    /**
     * Отобразить на лице улыбку.
     */
    public void smile() {
        System.out.printf("На %s появляется улыбка.\n", getName(Case.LOCATIVE));
    }

    public String getName(Case grammaticalCase) {
        String word;
        switch (grammaticalCase) {
            case GENITIVE:
                word = "лица";
                break;
            case ALLATIVE:
                word = "лицу";
                break;
            case LOCATIVE:
                word = "лице";
                break;
            case INSTRUMENTAL:
                word = "лицом";
                break;
            default:
                word = "лицо";
                break;
        }
        return String.format("%s %s", word, owner.getName(Case.GENITIVE));
    }
}
