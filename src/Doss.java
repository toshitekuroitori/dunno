import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Деревянная полка, используемая для сна.
 */
public class Doss implements Berth {
    private final HashSet<Shorty> occupants;

    Doss() {
        occupants = new HashSet<>();
    }

    /**
     * Количество коротышек, которые могут поместиться на полке.
     *
     * @return вместимость полки (два человека).
     */
    public int getCapacity() {
        return 2;
    }

    public List<Shorty> getOccupants() {
        return occupants.stream().collect(Collectors.toList());
    }

    public boolean addOccupant(Shorty shorty) {
        if (shorty == null) {
            System.out.printf("Барабашка безуспешно пробует разместиться на %s.", getName(Case.LOCATIVE));
            return false;
        }

        if (this.hasFreeSpace()) {
            System.out.printf("%s лежит на %s.\n", shorty.getName(), getName(Case.LOCATIVE));
            return occupants.add(shorty);
        } else {
            System.out.printf("Оказывается, %s уже занята.\n", getName());
            return false;
        }
    }

    public boolean removeOccupant(Shorty shorty) {
        return occupants.remove(shorty);
    }

    public String getName(Case grammaticalCase) {
        switch (grammaticalCase) {
            case NOMINATIVE:
                return "деревянная полка";
            case GENITIVE:
                return "деревянной полки";
            case LOCATIVE:
                return "деревянной полке";
            case INSTRUMENTAL:
                return "деревянной полкой";
            default:
                return "деревянную полку";
        }
    }
}
