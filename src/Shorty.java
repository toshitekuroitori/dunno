import java.util.Optional;

/**
 * Коротышки, будь то из Цветочного города, Солнечного города или даже с Луны.
 *
 * @author Николай Николаевич Носов
 */
class Shorty implements Visible {
    private final String name;
    private final String genitiveName;
    private final Curiosity curiosity;
    private final Face face;

    private Room location;
    private Berth bed;
    private Leanable support;

    Shorty(String name, String genitiveName, Curiosity curiosity) {
        this.name = name;
        this.genitiveName = genitiveName;
        this.curiosity = curiosity;
        this.face = new Face(this);
    }

    public String getName(Case grammaticalCase) {
        switch (grammaticalCase) {
            case GENITIVE:
                return genitiveName;
            default:
                return name;
        }
    }

    /**
     * Определяет, является ли коротышка любопытным.
     *
     * @return <code>true</code>, если уровень любопытства коротышки равен
     * или превышает <code>Curiosity.HIGH</code>.
     * @see Curiosity
     */
    public boolean isCurious() {
        return curiosity.compareTo(Curiosity.HIGH) >= 0;
    }

    /**
     * Определить, в каком помещении сейчас находится коротышка.
     *
     * @return местоположение коротышки.
     */
    public Room getLocation() {
        return this.location;
    }

    /**
     * Переместиться в другое помещение.
     *
     * @param room место назначения
     */
    public boolean moveTo(Room room) {
        if (room == null) {
            // Перемещение коротышки «в никуда» представляется абсурдным действом.
            System.out.printf("%s просят отправиться к чёртовой бабушке. %s вежливо отказывается.\n", getName(Case.ACCUSATIVE), getName());
            return false;
        } else {
            if (room.addOccupant(this)) {
                this.location = room;
                return true;
            } else return false;
        }
    }

    /**
     * Спрыгнуть с койки.
     */
    public void jumpFromBed() {
        if (getBed().isPresent()) {
            Berth bed = getBed().get();
            System.out.printf("%s спрыгивает с %s.\n",
                this.getName(), bed.getName(Case.GENITIVE));
            bed.removeOccupant(this);
        } else {
            System.out.printf("%s пытается спрыгнуть с койки, хотя ни на чём не лежит. Дурдом.\n", this.getName());
        }
    }

    /**
     * Подбежать к кому-нибудь.
     *
     * @param shorty тот, к кому нужно подбежать.
     */
    public void runTo(Shorty shorty) {
        if (this.equals(shorty)) {
            System.out.printf("%s замечает в своём сознании странное желание подбежать к самому себе. Жуть!\n", getName());
        } else {
            System.out.printf("%s подбегает к %s.\n", this.getName(), shorty.getName(Case.ALLATIVE));
        }
    }

    /**
     * Возвращает койку, на которой лежит коротышка.
     *
     * @return койка, которую сейчас занимает коротышка.
     */
    public Optional<Berth> getBed() {
        return Optional.ofNullable(this.bed);
    }

    public void jumpOn(Berth bed) {
        System.out.printf("%s пытается запрыгнуть на %s.\n", getName(), bed.getName(Case.ALLATIVE));
        if (bed.addOccupant(this)) {
            this.bed = bed;
        }
    }

    /**
     * Попятиться.
     *
     * @param fearfully пятиться в страхе — <code>true</code>, без страха — <code>false</code>.
     */
    public void recede(boolean fearfully) {
        System.out.printf("%s %s.\n", getName(), fearfully ? "в страхе пятится" : "пятится");
    }

    /**
     * Облокотиться на что-нибудь или прижаться к чему-нибудь.
     *
     * @param leanable то, к чему следует прижаться.
     */
    public void leanOn(Leanable leanable) {
        this.support = leanable;
        System.out.printf("%s прижимается к %s.\n", getName(), leanable.getName(Case.ALLATIVE));
    }

    /**
     * Перестать опираться на то, к чему сейчас прижимается коротышка.
     */
    public void leanOff() {
        if (support != null) {
            System.out.printf("%s отходит от %s.\n", getName(), support.getName(Case.GENITIVE));
            support = null;
        }
    }

    /**
     * Взглянуть на что-нибудь.
     *
     * @param something то, на что требуется посмотреть.
     */
    public void lookAt(Visible something) {
        System.out.printf(
            "%s смотрит на %s.\n",
            this.getName(),
            something.getName(Case.ACCUSATIVE));

        if (something.isFunny()) {
            laugh(false);
        }
    }

    /**
     * Подготовиться к какому-либо действию.
     *
     * @param action действие, записанное в форме инфинитива.
     */
    public void prepareTo(String action) {
        System.out.printf("%s готовится %s.\n", getName(), action.trim());
    }

    /**
     * @return Лицо данного коротышки.
     */
    public Face getFace() {
        return face;
    }

    /**
     * Рассмеяться.
     *
     * @param wittingly Смеяться невольно — <code>false</code>, вольно — <code>true</code>.
     */
    private void laugh(boolean wittingly) {
        getFace().smile();
        System.out.printf("%s %s.\n", getName(), wittingly ? "смеётся" : "невольно смеётся");
    }

    public void understand(String idea) {
        System.out.printf("%s понимает: %s.\n", getName(), idea);
    }
}
