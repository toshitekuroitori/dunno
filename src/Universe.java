import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;
import java.util.stream.Collectors;

/**
 * Главный класс программного пакета, управляющий поведением
 * всех описываемых сущностей в данной истории.
 * <p>
 * <blockquote>
 * «Увидев новоприбывшего, несколько самых любопытных коротышек соскочили
 * со своих полок и подбежали к нему. Незнайка в испуге попятился и,
 * прижавшись спиной к двери, приготовился защищаться. Разглядев его
 * измазанную физиономию, коротышки невольно рассмеялись. Незнайка понял,
 * что бояться не надо, и его лицо тоже расплылось в улыбке».
 * <div align="right">Н. Носов</div>
 * </blockquote>
 */
public class Universe {

    /**
     * Точка входа в программу.
     *
     * @param args параметры командной строки игнорируются.
     */
    public static void main(String[] args) {

        // Далее описывается каталажка — место действия сюжета — и её узники.
        Slammer slammer = new Slammer();
        Vector<Shorty> prisoners = new Vector<>();

        prisoners.add(new Shorty("Стрига", "Стриги", Curiosity.HIGH));
        prisoners.add(new Shorty("Вихор", "Вихра", Curiosity.HIGH));
        prisoners.add(new Shorty("Козлик", "Козлика", Curiosity.MEDIUM));
        prisoners.add(new Shorty("Мига", "Миги", Curiosity.HIGH));
        prisoners.add(new Shorty("Пестренький", "Пестренького", Curiosity.LOW));

        // Каждый из коротышек попадает в каталажку и прыгает на койку.
        prisoners.forEach((shorty -> {
            slammer.imprison(shorty);
            shorty.jumpOn(slammer.findFreeBerth().get());
            System.out.println();
        }));

        System.out.printf(
            "В каталажке находятся, свесив ноги с полок, коротышки %s.\n",
            enumerateShorties(slammer.getOccupants()));

        // Выбираем наиболее любопытных коротышек.
        Collection<Shorty> subjects = prisoners.stream()
            .filter(Shorty::isCurious)
            .collect(Collectors.toList());

        System.out.printf(
            "Наиболее любопытные коротышки в %s — это %s.\n",
            slammer.getName(Case.LOCATIVE), enumerateShorties(subjects));
        System.out.println();

        // Незнайка прилетает на Луну, где-то пачкается...
        Dunno dunno = new Dunno();
        dunno.getFace().makeDirty();

        // ...не оплачивает обед в ресторане и попадает в каталажку.
        slammer.imprison(dunno);
        System.out.println();

        // Любопытные коротышки начинают интересоваться новоприбывшим Незнайкой.
        subjects.forEach((shorty) -> {
            shorty.lookAt(dunno);
            shorty.jumpFromBed();
            shorty.runTo(dunno);
            System.out.println();
        });

        // Незнайке страшно, но он не показывает вида.
        // Во всяком случае, потом он так всем и расскажет.
        dunno.recede(true);
        dunno.leanOn(slammer.getDoor());
        dunno.prepareTo("защищаться");
        System.out.println();

        // Коротышки смотрят на грязное лицо Незнайки и смеются.
        subjects.forEach((shorty -> {
            shorty.lookAt(dunno.getFace());
            System.out.println();
        }));

        // Незнайка успокаивается.
        dunno.understand("бояться не надо");
        dunno.getFace().smile();
        dunno.leanOff();
    }

    /**
     * Перечислить имена коротышек через запятую.
     *
     * @param shorties список коротышек
     * @return строка, содержащая имена коротышек, разделённые запятыми.
     */
    private static String enumerateShorties(Collection<Shorty> shorties) {
        return String.join(", ", shorties.stream()
            .map(Visible::getName)
            .collect(Collectors.toList()));
    }
}
