import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Помещение.
 */
public interface Room extends Visible {
    /**
     * Добавить коротышку в помещение.
     *
     * @param shorty коротышка, которого нужно поместить в комнату.
     * @return <code>true</code>, если коротышка не <code>null</code>, ещё
     * не находился в комнате и ему хватило места. Во всех остальных
     * случаях метод возвращает <code>false</code>.
     */
    boolean addOccupant(Shorty shorty);

    /**
     * Удалить коротышку из комнаты.
     *
     * @param shorty коротышка, которого нужно удалить из комнаты.
     * @return <code>true</code>, если указанный коротышка был в комнате,
     * иначе <code>false</code>.
     */
    boolean removeOccupant(Shorty shorty);

    /**
     * @return Дверь, ведущая в помещение. Предполагается, что в помещение
     * ведёт ровно одна дверь.
     */
    Door getDoor();

    /**
     * @return Список коек, расположенных в помещении.
     */
    Collection<Berth> getBerths();

    /**
     * @return Список людей, находящихся в комнате.
     */
    default Collection<Shorty> getOccupants() {
        return getBerths().stream().flatMap(berth -> berth.getOccupants().stream()).collect(Collectors.toList());
    }

    /**
     * @return Экземпляр <code>Optional</code>, содержащий найденную свободную койку,
     * или пустой <code>Optional</code>, если все койкоместа заняты.
     */
    default Optional<Berth> findFreeBerth() {
        return getBerths().stream().filter(Berth::hasFreeSpace).findFirst();
    }

    /**
     * @return Общее количество койкомест в помещении.
     */
    default int getCapacity() {
        return getBerths().stream().map(Berth::getCapacity).reduce(0, Integer::sum);
    }
}
