import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

class Slammer implements Room {
    private static final int NUMBER_OF_BEDS = 10;

    private final HashSet<Shorty> prisoners;
    private final Vector<Doss> dosses;
    private final Door door;

    Slammer() {
        this.door = new Door(this);
        this.prisoners = new HashSet<>();

        this.dosses = new Vector<>();
        for (int i = 0; i < NUMBER_OF_BEDS; i++) {
            dosses.add(new Doss());
        }
    }

    void imprison(Shorty shorty) {
        shorty.moveTo(this);
    }

    public String getName(Case grammaticalCase) {
        switch (grammaticalCase) {
            case NOMINATIVE:
                return "каталажка";
            case GENITIVE:
                return "каталажки";
            case ALLATIVE:
            case ACCUSATIVE:
                return "каталажку";
            case INSTRUMENTAL:
                return "каталажкой";
            default:
                return "каталажке";
        }
    }

    public Door getDoor() {
        return this.door;
    }

    public Collection<Berth> getBerths() {
        // Во имя инкапсуляции заклинаем этот метод возвращать скопированный
        // вектор полок, чтобы он не был изменен извне.
        return dosses.stream().collect(Collectors.toList());
    }

    public List<Shorty> getOccupants() {
        // Во имя инкапсуляции заклинаем этот метод возвращать скопированный
        // список коротышек, чтобы данные не были изменены без нашего ведома.
        return prisoners.stream().collect(Collectors.toList());
    }

    /**
     * @return <code>true</code>, если в каталажке есть свободное место.
     */
    private boolean hasFreePlace() {
        return findFreeBerth().isPresent();
    }

    public boolean addOccupant(Shorty shorty) {
        if (shorty == null) {
            // Нельзя добавить в комнату null'евого коротышку.
            System.out.printf("Призрак Нуль пытается проникнуть в %s. %s начинает сходить с ума.\n", this.getName(Case.ALLATIVE), this.getName());
            return false;
        }

        if (this.hasFreePlace()) {
            boolean heWasNew = prisoners.add(shorty);

            if (heWasNew) {
                System.out.printf("%s попадает в %s.\n", shorty.getName(), this.getName(Case.ACCUSATIVE));
            } else {
                // В комнате уже был этот коротышка. Сделаем вид, что всё (почти) нормально.
                System.out.printf("%s почувствовал что-то неладное, когда его выгнали из %s и снова туда поместили.\n",
                    shorty.getName(), getName(Case.GENITIVE));
            }

            return heWasNew;
        } else {
            System.out.printf("К сожалению, в %s не хватило места для ещё одного коротышки.\n", this.getName(Case.LOCATIVE));
            return false;
        }
    }

    // Удалить человека из комнаты
    public boolean removeOccupant(Shorty shorty) {
        if (shorty == null) {
            System.out.printf("Вы когда-нибудь видели, как Никто уходит из %s? Вот и я не видел.",
                getName(Case.GENITIVE));
            return false;
        }

        boolean heWasHere = prisoners.remove(shorty);

        if (heWasHere) {
            System.out.printf("Вот и не стало в %s коротышки по имени %s.\n",
                this.getName(Case.LOCATIVE), shorty.getName());
        } else {
            // Коротышки не было в комнате.
            System.out.printf("Только что %s попробовал покинуть %s, где его, собственно говоря, и не было.",
                shorty.getName(Case.ACCUSATIVE), this.getName());
        }

        return heWasHere;
    }
}
