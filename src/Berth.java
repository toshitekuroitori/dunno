import java.util.Collection;

/**
 * Койка — место для лежания.
 */
public interface Berth extends Visible {
    /**
     * @return Количество спальных мест на койке.
     */
    int getCapacity();

    /**
     * @return Список лежащих на койке.
     */
    Collection<Shorty> getOccupants();

    /**
     * @return Есть ли свободное место на койке.
     */
    default boolean hasFreeSpace() {
        return getOccupants().size() < getCapacity();
    }

    /**
     * Добавить коротышку на койку.
     */
    boolean addOccupant(Shorty shorty);

    /**
     * Убрать коротышку с койки.
     */
    boolean removeOccupant(Shorty shorty);
}
