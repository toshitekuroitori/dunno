/**
 * Дверь, ведущая в то или иное помещение.
 */
public class Door implements Leanable {
    /**
     * Помещение, куда ведёт дверь.
     */
    private final Room room;

    Door(Room room) {
        this.room = room;
    }

    /**
     * @return Комната, в которую ведёт дверь.
     */
    private Room getRoom() {
        return room;
    }

    public String getName(Case grammaticalCase) {
        String word;
        switch (grammaticalCase) {
            case NOMINATIVE:
            case ACCUSATIVE:
                word = "дверь";
                break;
            case INSTRUMENTAL:
                word = "дверью";
                break;
            default:
                word = "двери";
                break;
        }
        return String.format("%s в %s", word, getRoom().getName(Case.ACCUSATIVE));
    }
}
